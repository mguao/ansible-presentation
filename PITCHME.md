
# ANSIBLE
### Automation for Everyone

#HSLIDE
## MARLON GUAO
#### CloudOps Automation  

#HSLIDE

![alt text](https://img.deusm.com/informationweek/august/automation-shutterstock489.jpg)

#HSLIDE

# What is Automation?
#HSLIDE
"the use of largely automatic equipment in a system of manufacturing or other production process."

"unemployment due to the spread of automation"

#HSLIDE
![automation](https://www.recargacocheselectricos.com/wp-content/uploads/tesla-fabrica-2.jpg)

#HSLIDE

"is the technology by which a process or procedure is performed without human assistance"

![automate](https://www.businessworldit.com/wp-content/uploads/2018/04/Why-Business-Process-Automation-Is-Important.jpg)

#HSLIDE
# Automation is not NEW

#HSLIDE
# Why Automation?

#HSLIDE
![why automate](https://image.slidesharecdn.com/automationconfessions-141020100239-conversion-gate01/95/confessions-of-an-automation-addict-39-638.jpg?cb=1493156512)

#HSLIDE

### "I will always choose a lazy person to do a difficult job because a lazy person will find an easy way to do it"

## -- Bill Gates

#HSLIDE

### BENEFITS for the customer
 * improve customer experience due to better consistency
 * improve response time
 * lower costs

#HSLIDE
### BENEFITS for the service provider
 * increases the lifetime value of customer and brand loyalty
 * empowers organizations to optimize internal resource allocation, capitalization and opportunity costs.
 * enable businesses to scale while ensuring consistency in customer experience.

#HSLIDE
## What do we automate?
* Repeatable tasks
* low-skill processes

#HSLIDE
# What is Ansible?
#HSLIDE

"Ansible is a universal language, unraveling the mystery of how work gets done. Turn tough tasks into repeatable playbooks. Roll out enterprise-wide protocols with the push of a button."
#HSLIDE
# Why Ansible?
#HSLIDE
* All-in-one
* Lower Learning Curve
* No Agents
* Automate Now!
#HSLIDE
# How Ansible works?
#HSLIDE
![how ansible](https://image.slidesharecdn.com/s111013delarosa-170508174810/95/automated-outofband-management-with-ansible-and-redfish-26-638.jpg?cb=1494265856)
#HSLIDE
# USE CASE

#HSLIDE
# THANK YOU
